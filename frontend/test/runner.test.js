import { render, screen } from "./test-utils";

test("Jest runs simple test", () => {
  expect(true).toBe(true);
});

test("react-testing-library confirms component structure", () => {
  render(<div>Hello react-testing-library!</div>);

  const message = () => screen.getByText(/Hello react-testing-library/i);

  expect(message).not.toThrow();
  expect(message()).toBeInTheDocument();
});
