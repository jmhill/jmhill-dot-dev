import * as React from "react";
import tw from "twin.macro";
import Footer from "../Footer";
import Nav from "../Nav";

const navItems = [
  { name: "Articles", path: "/articles" },
  { name: "Projects", path: "/projects" },
  { name: "About", path: "/about" },
];

const title = "Justin M. Hill";

const Separator = tw.div`border-b-2 border-gray-200`;

const BaseLayout = ({
  children,
}: {
  children: React.ReactElement;
}): JSX.Element => (
  <div tw="flex flex-col h-screen p-4">
    <div tw="space-y-2">
      <Nav title={title} items={navItems}></Nav>
      <Separator />
    </div>
    <div tw="flex-grow my-2">{children}</div>
    <Footer />
  </div>
);

export default BaseLayout;
