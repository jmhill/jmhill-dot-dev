import Image from "next/image";
import "twin.macro";

const Avatar = ({ url, name }) =>
  url ? (
    <Image src={url} alt={name} width={48} height={48} tw="rounded" />
  ) : (
    <div tw="border-2 w-min">No Image</div>
  );

export default Avatar;
