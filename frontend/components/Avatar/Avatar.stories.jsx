import React from "react";

import Avatar from "./Avatar.jsx";

const story = {
  title: "Base/Avatar",
  component: Avatar,
  argTypes: {
    // example for color picker
    //backgroundColor: { control: "color" },
  },
};

export default story;

const Template = (args) => <Avatar {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  name: "Bill Murray",
  url: "/fillmurray600x600.jpg",
};

export const NoUrlProvided = Template.bind({});
NoUrlProvided.args = {
  name: "Mysterio",
};
