import React from "react";

import Article from "./Article.jsx";
import { LongText as MarkdownStories } from "../MarkdownContent/MarkdownContent.stories.jsx";

const story = {
  title: "Page Types/Article",
  component: Article,
  argTypes: {
    // example for color picker
    //backgroundColor: { control: "color" },
  },
};

export default story;

const Template = (args) => <Article {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  title: "A Sample Article",
  content: MarkdownStories.args.children,
  description: "This is a sample article - and this is the description.",
  publishedAt: "2021-08-26T18:05:31.697Z",
  author: {
    name: "Bill Murray",
    picture: "/fillmurray600x600.jpg",
    bio: "Bill Murray is a pretty cool dude. One time he went to the movies and bought tickets for everybody behind him in line.",
  },
};
