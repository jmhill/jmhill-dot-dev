import * as React from "react";
import MarkdownContent from "../MarkdownContent";
import tw from "twin.macro";
import Byline from "../Byline";
import Moment from "react-moment";

const markdownStyles = {
  h1: tw`text-4xl font-semibold`,
  h2: tw`text-3xl font-semibold`,
  h3: tw`text-2xl font-semibold`,
  h4: tw`text-xl font-semibold`,
  h5: tw`text-lg font-semibold`,
  h6: tw`text-base font-semibold`,
  p: tw`text-base font-serif`,
  a: tw`underline hover:text-blue-400`,
  ul: tw`list-disc px-8`,
  ol: tw`list-decimal px-8`,
};

const Article = ({ title, description, content, author, publishedAt }) => (
  <article tw="space-y-6 max-w-prose">
    <h1 tw="text-5xl font-bold">{title}</h1>
    <aside tw="italic text-xl font-light">
      {publishedAt ? (
        <span tw="italic font-normal">
          <time dateTime={publishedAt}>
            <Moment format="MMM Do YYYY">{publishedAt}</Moment>
            {" - "}
          </time>
        </span>
      ) : null}
      {description}
    </aside>
    <section css={markdownStyles} tw="space-y-4">
      <MarkdownContent>{content}</MarkdownContent>
    </section>
    <Byline
      name={author.name}
      pictureUrl={author.picture}
      shortBio={author.bio}
    ></Byline>
  </article>
);

export default Article;
