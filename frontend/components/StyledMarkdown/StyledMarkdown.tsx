import tw from "twin.macro";
import MarkdownContent from "../MarkdownContent";

const markdownStyles = {
  h1: tw`text-4xl font-semibold`,
  h2: tw`text-3xl font-semibold`,
  h3: tw`text-2xl font-semibold`,
  h4: tw`text-xl font-semibold`,
  h5: tw`text-lg font-semibold`,
  h6: tw`text-base font-semibold`,
  p: tw`text-base font-serif`,
  a: tw`underline hover:text-blue-400`,
  ul: tw`list-disc px-8`,
  ol: tw`list-decimal px-8`,
};

type StyledMarkdownProps = {
  children: string;
};

const StyledMarkdown = ({ children }: StyledMarkdownProps): JSX.Element => (
  <div css={markdownStyles} tw="space-y-4">
    <MarkdownContent>{children}</MarkdownContent>
  </div>
);

export default StyledMarkdown;
