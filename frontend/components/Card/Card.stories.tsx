import * as React from "react";
import "twin.macro";
import Link from "next/link";
import CardComponent from "./Card";

const story = {
  title: "Base/Card",
  component: CardComponent,
};

export default story;

const Template = (args) => (
  <div tw="md:w-1/2">
    <CardComponent {...args} />
  </div>
);

export const Card = Template.bind({});
Card.args = {
  title: "My card",
  image: { url: "/mountain-demo-unsplash.jpg", width: 200, height: 200 },
  children: (
    <div tw="space-y-2">
      <p>
        Hello world, this is some card content. Usually this would be the
        description of the associated article
      </p>
      <p tw="font-light">
        <Link href="https://google.com">Read more...</Link>
      </p>
    </div>
  ),
};
