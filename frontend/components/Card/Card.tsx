import * as React from "react";
import Image from "next/image";
import { getStrapiMedia } from "../../lib/media";
import "twin.macro";

export interface StrapiImage {
  url: string;
  alternativeText: string;
  width: number;
  height: number;
}

export type CardProps = {
  title: string;
  image: StrapiImage;
  children: React.ReactChild;
};

const Card = ({ title, image, children }: CardProps): JSX.Element => {
  const loader = () => {
    return getStrapiMedia(image);
  };

  return (
    <div tw="flex flex-col shadow-lg border border-gray-100">
      <div tw="w-full">
        {image.url ? (
          <Image
            loader={loader}
            src={image.url}
            alt={image.alternativeText}
            layout="responsive"
            width={image.width}
            height={image.height}
            objectFit="cover"
            tw="bg-gray-100"
          />
        ) : null}
      </div>
      <div tw="flex flex-col w-full p-4 space-y-4">
        <div tw="text-lg font-bold">{title}</div>
        {children}
      </div>
    </div>
  );
};

export default Card;
