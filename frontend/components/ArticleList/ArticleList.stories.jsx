import * as React from "react";

import ArticleList from ".";

const story = {
  title: "Base/ArticleList",
  component: ArticleList,
  argTypes: {},
};

export default story;

const Template = (args) => <ArticleList {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  articles: [
    {
      title: "A Test article",
      description: "This is just a test article",
      author: {
        name: "Bill Murray",
        picture: "/fillmurray600x600.jpg",
      },
      slug: "/a-test-article",
      image: { url: "/mountain-demo-unsplash.jpg", height: 2000, width: 2000 },
    },
    {
      title: "Another test article",
      description: "Here's another cool story about bill",
      author: {
        name: "Bill Murray",
        picture: "fillmurray600x600.jpg",
      },
      slug: "another-test-article",
      image: { url: "/fillmurray600x600.jpg", height: 600, width: 600 },
    },
  ],
};
