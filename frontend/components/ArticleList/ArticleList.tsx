import * as React from "react";
import "twin.macro";
import { StrapiImage, CardProps } from "../Card";
import CardGrid from "../CardGrid";

export interface ArticleListItem {
  title: string;
  image: StrapiImage;
  description: string;
}

export interface ArticleListProps {
  articles: ArticleListItem[];
}

const ArticleList = ({ articles }: ArticleListProps): JSX.Element => {
  const cards = articles.map((article): CardProps => {
    return {
      title: article.title,
      children: <p>{article.description}</p>,
      image: article.image,
    };
  });
  return <CardGrid cards={cards} />;
};
export default ArticleList;
