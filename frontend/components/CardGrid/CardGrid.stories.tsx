import * as React from "react";

import CardGrid from ".";

const story = {
  title: "Base/CardGrid",
  component: CardGrid,
  argTypes: {},
};

export default story;

const Template = (args) => <CardGrid {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  cards: [
    {
      title: "A Test article",
      image: {
        url: "/mountain-demo-unsplash.jpg",
        alternativeText: "testing",
        height: 2000,
        width: 2000,
      },
      children: <p>Hello World</p>,
    },
    {
      title: "Another test article",
      image: {
        url: "/fillmurray600x600.jpg",
        alternativeText: "testing",
        height: 600,
        width: 600,
      },
      children: <p>Try this again</p>,
    },
  ],
};
