import "twin.macro";
import Card, { CardProps } from "../Card";

type CardGridProps = {
  cards: CardProps[];
};

const CardGrid = ({ cards }: CardGridProps): JSX.Element => {
  const cardItems = cards.map((cardProps: CardProps) => (
    <Card key={cardProps.title} title={cardProps.title} image={cardProps.image}>
      {cardProps.children}
    </Card>
  ));
  return (
    <div tw="grid gap-4 md:grid-cols-1 lg:grid-cols-2 xl:grid-cols-3">
      {cardItems}
    </div>
  );
};

export default CardGrid;
