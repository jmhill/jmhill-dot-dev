import React from "react";

import Banner from "./Banner.jsx";

const story = {
  title: "Base/Banner",
  component: Banner,
  argTypes: {
    // example for color picker
    //backgroundColor: { control: "color" },
  },
};

export default story;

const Template = (args) => (
  <div>
    <Banner {...args} />
  </div>
);

export const Standard = Template.bind({});
Standard.args = {
  name: "Some mountains",
  url: "/mountain-demo-unsplash.jpg",
};
