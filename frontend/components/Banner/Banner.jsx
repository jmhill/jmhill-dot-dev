import Image from "next/image";
import "twin.macro";

const Banner = ({ url, name }) => (
  <div tw="relative h-80 lg:h-screen">
    <Image src={url} alt={name} layout="fill" objectFit="cover" />
  </div>
);

export default Banner;
