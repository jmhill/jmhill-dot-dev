import Link from "next/link";
import "twin.macro";

interface NavItem {
  name: string;
  path: string;
}

interface NavProps {
  title: string;
  items: NavItem[];
}

const Nav = ({ title, items }: NavProps): JSX.Element => (
  <div tw="flex flex-col md:flex-row justify-between">
    <span tw="font-medium text-lg">
      <Link href="/">{title}</Link>
    </span>
    <nav>
      <ul tw="flex flex-row space-x-4">
        {items.map((item) => (
          <li key={item.name}>
            <Link href={item.path}>{item.name}</Link>
          </li>
        ))}
      </ul>
    </nav>
  </div>
);

export default Nav;
