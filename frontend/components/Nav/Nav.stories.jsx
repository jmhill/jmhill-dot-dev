import React from "react";

import Nav from "./Nav";

const story = {
  title: "Base/Nav",
  component: Nav,
  argTypes: {
    // example for color picker
    //backgroundColor: { control: "color" },
  },
};

export default story;

const Template = (args) => <Nav {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  title: "My Site",
  items: [
    {
      name: "Home",
      path: "/",
    },
    {
      name: "About",
      path: "/about",
    },
    {
      name: "Contact",
      path: "/contact",
    },
  ],
};
