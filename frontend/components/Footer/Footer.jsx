import * as React from "react";
import "twin.macro";

const Footer = () => (
  <footer tw="flex flex-row w-full justify-center items-center space-x-6 p-4">
    <div>&copy; {new Date().getFullYear()} Justin M. Hill</div>
    <div>
      <a href="mailto:justin@jmhill.dev">Contact</a>
    </div>
  </footer>
);

export default Footer;
