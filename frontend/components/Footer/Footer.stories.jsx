import * as React from "react";
import Footer from "./Footer.jsx";

const story = {
  title: "Base/Footer",
  component: Footer,
  argTypes: {},
};

export default story;

const Template = (args) => <Footer {...args} />;

export const BasicFooter = Template.bind({});
BasicFooter.args = {};
