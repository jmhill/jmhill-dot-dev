import React from "react";

import Byline from "./Byline.jsx";

const story = {
  title: "Base/Byline",
  component: Byline,
  argTypes: {
    // example for color picker
    //backgroundColor: { control: "color" },
  },
};

export default story;

const Template = (args) => <Byline {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  name: "Bill Murray",
  pictureUrl: "/fillmurray600x600.jpg",
  bioLink: "https://google.com",
  shortBio:
    "Bill Murray is one of the greatest actors of all time. Anybody who thinks otherwise is a fool.",
};

export const WithNoImage = Template.bind({});
WithNoImage.args = {
  name: "John Doe",
  bioLink: "https://google.com",
  shortBio: "this is an anonymous user.",
};
