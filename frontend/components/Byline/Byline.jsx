import Moment from "react-moment";
import Link from "next/link";
import Avatar from "../Avatar";
import "twin.macro";

const Byline = ({
  name = "Unknown Author",
  pictureUrl,
  bioLink = "#",
  shortBio,
}) => (
  <div tw="flex flex-col items-start space-y-1">
    <span tw="text-xl">
      Posted by{" "}
      <Link href={bioLink} passHref>
        <a tw="font-semibold">{name}</a>
      </Link>
    </span>
    <div tw="flex flex-row items-start space-x-1">
      <div tw="flex-shrink-0">
        {pictureUrl ? <Avatar url={pictureUrl} name={name} /> : null}
      </div>
      <span tw="text-sm font-light">{shortBio}</span>
    </div>
  </div>
);

export default Byline;
