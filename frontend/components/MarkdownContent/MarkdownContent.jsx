import ReactMarkdown from "react-markdown";

const MarkdownContent = ({ children }) => (
  <ReactMarkdown>{children}</ReactMarkdown>
);

export default MarkdownContent;
