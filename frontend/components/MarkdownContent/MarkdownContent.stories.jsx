import React from "react";

import MarkdownContent from "./MarkdownContent.jsx";

const story = {
  title: "Base/MarkdownContent",
  component: MarkdownContent,
  argTypes: {
    // example for color picker
    //backgroundColor: { control: "color" },
  },
};

export default story;

const Template = (args) => (
  <section>
    <MarkdownContent {...args} />
  </section>
);

export const Standard = Template.bind({});
Standard.args = {
  children:
    `## Demo  \n` +
    `This is some sample markdown content  \n` +
    `Spaces are important  \n` +
    `Because markdown needs 2 spaces at the end of a line  `,
};

export const LongText = Template.bind({});
LongText.args = {
  children: `
  # Aequor sub sed quam cernenda me mihi

  ## Vulnus nec atras quondam dederat imperat gessit

  Lorem markdownum decoram primumque indicium ad veros liquidas nomina pererrato,
  riget nec illic ventos insequitur [simul](http://longius.net/per). Edidit sive
  ego premebat illa, extemplo, cum humus *fecitque* virgo. Iubasque pro et
  attulerat diversa sedent, stringitur flammam micant, feremus scelus et maior,
  arma.

      var netmaskArrayFile = smartphoneMainframeDisplay(double_laptop_meta(
              metal_irc), 4 + time, cpl_mountain_copy(phpDdr, folder_management +
              screenshot, 3));
      memeBloatware = fddiRom - mapServerAdd;
      if (myspaceSidebarParity(2 + -3, tft_search_down) < 5) {
          affiliate += 5;
      }
      if (42 != 944698) {
          masterZif += webLteHost;
          beta.extensionDigital.down_push_wiki(parity, record_caps.white.bin(
                  rdram, xml, raw_syn), 1);
      }

  ## Mactare per in humum

  Fronte deos volebat ruinam amaris. Si perque presserat contigit posse umbramque
  portae significant, equos ora simul et.

  1. Usus suos mediam a ille raptum dat
  2. Quam saevit sideribus montanum territa iubenti cinctasque
  3. Ambiguo manes quaecumque vitiorum fert levatus comitantibus
  4. Laedar peremi


  ## Tuto mihi

  Vel fide nam ait carinam nequeo nec in arma fides. More in lumen, vota
  pulcherrimus recipit hic percutit genu agmine feras coniunx alis
  [oris](http://www.illi-annus.io/confessisomnia.php).

      expression_memory += microcomputer_media / ppi;
      twitter_expansion += pingProgressive.primaryCore.desktopHdmi(
              publishing_clob_half + memory_oem_warm) - affiliate_gigaflops;
      address.del += debugger + drive + dfs_rw_hsf + sharewareLinkedin;

  Quoque gementis obicit. **Non** agmen adstat huic, non at gradibus vertitur
  vetus, fraternis. Corpora pronus. [Cum dixit](http://dedissent-novat.net/ab.php)
  cruor: est monuit pro placent remorata lapidum optasse, sinu **erat** abire
  sumptis. Serpere dixit *aera* fingi e palmis annis sustinui, et media luminibus
  iuvenilior gemit limite.
  `,
};
