import { StrapiImage } from "../components/Card";
import { fetchAPI } from "../lib/api";
import { getStrapiMedia } from "../lib/media";
import StyledMarkdown from "../components/StyledMarkdown";
import Image from "next/image";
import "twin.macro";

type AboutPageData = {
  bio: string;
  photo: StrapiImage;
};

type AboutPageProps = {
  aboutPage: AboutPageData;
};

const About = ({ aboutPage }: AboutPageProps): JSX.Element => {
  const { bio, photo } = aboutPage;

  if (!photo) {
    return <p>{bio}</p>;
  }

  const loader = () => getStrapiMedia(photo);
  return (
    <div tw="py-2 md:w-1/2 mx-auto">
      <div tw="flex flex-col justify-center items-center">
        <div tw="w-1/2">
          <Image
            loader={loader}
            src={photo.url}
            alt={photo.alternativeText}
            layout="responsive"
            width={photo.width}
            height={photo.height}
            objectFit="cover"
            tw="rounded-full"
          />
        </div>
        <StyledMarkdown>{bio}</StyledMarkdown>
      </div>
    </div>
  );
};

export default About;

export async function getStaticProps(): Promise<{
  props: {
    aboutPage: AboutPageData;
  };
  revalidate: number;
}> {
  const aboutPage: AboutPageData = await fetchAPI("/about");

  return {
    props: { aboutPage },
    revalidate: 1,
  };
}
