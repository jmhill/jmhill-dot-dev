import React from "react";
import Seo from "../components/seo";
import { fetchAPI } from "../lib/api";
import ArticleList, { ArticleListItem } from "../components/ArticleList";
import "twin.macro";

type HomeProps = {
  articles: ArticleListItem[];
  categories: Category[];
  homepage: HomePageData;
};

type Category = {
  name: string;
  slug: string;
};

type HomePageData = {
  seo: SeoData;
  hero: {
    title: string;
  };
  welcomeMessage: string;
};

type SeoData = {
  metaTitle: string;
  metaDescription: string;
  shareImage: string;
};

const Home = ({ articles, categories, homepage }: HomeProps): JSX.Element => {
  // const navItems = categories.map((category) => ({
  //   name: category.name,
  //   path: `/${category.slug}`,
  // }));

  return (
    <div tw="space-y-4">
      <Seo seo={homepage.seo} />
      <h2 tw="text-2xl font-semibold">Welcome</h2>
      <div>{homepage.welcomeMessage}</div>
      <h2 tw="text-2xl font-semibold">Recent Articles</h2>
      <ArticleList articles={articles}></ArticleList>
    </div>
  );
};

export async function getStaticProps(): Promise<{
  props: {
    articles: ArticleListItem[];
    categories: Category[];
    homepage: HomePageData;
  };
  revalidate: number;
}> {
  // Run API calls in parallel
  const [articles, categories, homepage] = await Promise.all([
    fetchAPI("/articles"),
    fetchAPI("/categories"),
    fetchAPI("/homepage"),
  ]);

  return {
    props: { articles, categories, homepage },
    revalidate: 1,
  };
}

export default Home;
