# jmhill.dev

WIP

## Running in Development

This is a WIP, will clean up soon:

### Option 1: npm run develop

This was the script that came with the strapi-next starter - takes care of
running the strapi app first, and waiting on the api endpoints to be up
so that next.js can query the CMS APIs in order to generate pages using
getStaticProps. Currently, if the Strapi CMS is not running and available,
the next.js build will fail. Would obviously prefer this to be a little more
resilient to failure. If needed, a devcontainer has been provided to open the
entire project in vs code with access to nodejs/npm, and then run 
`npm run develop` in the project root.

### Option 2: docker compose up

Runs the next.js and and strapi API projects as separate services using docker
compose. Currently a little finicky - the docker build for the next.js app
runs the next.js build process, which as noted above relies on being able to
access the CMS to generate initial pages. However, I've altered the next.js
Dockerfile to support a multistage build with a dev stage that docker compose
can now use to get the container up and running for dev purposes at least.